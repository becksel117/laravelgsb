<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Rendez-vous</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    </head>

<body>
        
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

             <li class="nav-item">
                 <a href="{{URL::route("Connexion")}}">Déconnexion</a>
            </li>
        </ul>     
    </nav> 
    
     <form action="/Rdv" method="post">
     {{ csrf_field() }}
        <input type="text" name="medId" placeholder="Identifiant du Médecin">
        <input type="text" name="date" placeholder="Date du RDV">
        <input type="text" name="prodId" placeholder="Identifiant du produit à présenter">
        <input type="submit" value="Valider le Rdv">
    </form>
    
    
    
</body>
</html>