<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Rendez-vous</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    </head>

<body>
        
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

             <li class="nav-item">
                 <a href="{{URL::route("Connexion")}}">Déconnexion</a>
            </li>
        </ul>     
    </nav> 
    
    <div class="container">
            <h2 class="text-center">Informations sur les Rendez-vous</h2>
            <br>
                <table class="table table-striped ">
            <br>
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Médecin</th>
                        <th scope="col">Visiteur</th>
                        <th scope="col">Date</th>
                        <th scope="col">Produit</th>
                        <th>
                            <a href="{{URL::route("AddRdv")}}"> 
                                <input type="image" id="addRdv" alt="ajouter"
       src="Images/iconeAdd.png" height="35" width="35">
                            </a>
                        </th>
                     
                    </tr>
                </thead>
             <tbody>
                @foreach($Rdv as $item)
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">{{$item->RdvMedId}}</th>
                        <th scope="col">{{$item->RdvVisId}}</th>
                        <th scope="col">{{$item->RdvDate}}</th>
                        <th scope="col">{{$item->ProId}}</th>
                        <th>
                            <a href="{{URL::route("UpdateRdv", ['idMed' => $item->RdvMedId, 'idVis' => $item->RdvVisId, 'date' => $item->RdvDate])}}"> 
                                <input type="image" id="updateRdv" alt="éditer"
       src="Images/iconeUpdate.png" height="35" width="35">
                            </a>
                            
                            <a href="{{URL::route("DeleteRdv", ['idMed' => $item->RdvMedId, 'idVis' => $item->RdvVisId, 'date' => $item->RdvDate])}}"> 
                                <input type="image" id="deleteRdv" alt="supprimer"
       src="Images/iconeDelete.png" height="35" width="35">
                            </a>
                            
                        </th>
                        
                    </tr>
                    </thread>
                @endforeach
                </tbody>
                </table>
        </div> 
    
</body>
</html>

