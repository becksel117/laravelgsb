<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Medecin</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    </head>

<body>
        
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

             <li class="nav-item">
                 <a href="{{URL::route("Connexion")}}">Connexion</a>
            </li>
        </ul>     
    </nav> 
    
    <br>
        <div class="container">
            <h2 class="text-center">Informations sur les médecins</h2>
            <br>
                <table class="table table-striped ">
            <br>
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Nom</th>
                        <th scope="col">Prénom</th>
                        <th scope="col">Mail</th>
                        <th scope="col">Spécialisation</th>
                        <th scope="col">Cabinet</th>
                        <th>
                            <a href="{{URL::route("AddMedecin")}}"> 
                                <input type="image" id="addMedecin" alt="ajouter"
       src="Images/iconeAdd.png" height="35" width="35">
                            </a>
                        </th>
                        
                    </tr>
                </thead>
                
            
            <tbody>
                @foreach($Medecin as $item)
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">{{$item->id}}</th>
                        <th scope="col">{{$item->nom}}</th>
                        <th scope="col">{{$item->prenom}}</th>
                        <th scope="col">{{$item->mail}}</th>
                        <th scope="col">{{$item->specialisation}}</th>
                        <th scope="col">{{$item->cabId}}</th>
                        <th>
                            
                           
                            <a href="{{URL::route("UpdateMedecin", ['id' => $item->id])}}"> 
                                <input type="image" id="updateMedecin" alt="éditer"
       src="Images/iconeUpdate.png" height="35" width="35">
                            </a>
                            
                            <a href="{{URL::route("DeleteMedecin", ['id' => $item->id])}}"> 
                                <input type="image" id="deleteMedecin" alt="supprimer"
       src="Images/iconeDelete.png" height="35" width="35">
                            </a>
                            
                        </th>
                    
                        
                    </tr>
                    </thread>
                @endforeach
                </tbody>
                </table>
        </div>   
    
</body>
</html>

