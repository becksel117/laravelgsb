<!DOCTYPE html>
<html lang="en">
    <head>
        <title>UpdateCabinet</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    </head>

<body>
        
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

             <li class="nav-item">
                 <a href="{{URL::route("Connexion")}}">Connexion</a>
            </li>
        </ul>     
    </nav> 
    
    <form method="POST" action="">
            <h3 class="text-center text" name="txt">Editer un Cabinet</h3>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 offset-sm-3 mt-5">
                        
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">id : </span>
                            </div>
                            <input type="number" class="form-control" name="id" value="" required="">
                        </div>   
                        
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">nom : </span>
                            </div>
                            <input type="text" class="form-control" name="nom" value="" required="">
                        </div>   
                        
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">région : </span>
                            </div>
                            <input type="text" class="form-control" name="region" value="" required="">
                        </div>  
                        
                        

                        <input type="submit" name="valid" value="valider">
                        
                    </div>

                </div>
            </div>
        </form>
    
</body>
</html>
