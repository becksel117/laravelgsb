<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Cabinet</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    </head>

<body>
        
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

             <li class="nav-item">
                 <a href="{{URL::route("Connexion")}}">Connexion</a>
            </li>
        </ul>     
    </nav> 
    
    <br>
        <div class="container">
            <h2 class="text-center">Informations sur les cabinets</h2>
            <br>
                <br>
                <table class="table table-striped ">
            
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Nom</th>
                        <th scope="col">Région</th>
                        <th>
                            <a href="{{URL::route("AddCabinet")}}"> 
                                <input type="image" id="addCabinet" alt="ajouter"
       src="Images/iconeAdd.png" height="35" width="35">
                            </a>
                        </th>
                        
                    </tr>
                </thead>
                
            
                
                    
                        
                <tbody>
                @foreach($Cabinet as $item)
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">{{$item->id}}</th>
                        <th scope="col">{{$item->nom}}</th>
                        <th scope="col">{{$item->region}}</th>
                        <th>
                           
                            <a href="{{URL::route("UpdateCabinet", ['id' => $item->id])}}"> 
                                <input type="image" id="updateCabinet" alt="éditer"
       src="Images/iconeUpdate.png" height="35" width="35">
                            </a>
                            
                            <a href="{{URL::route("DeleteCabinet", ['id' => $item->id])}}"> 
                                <input type="image" id="deleteCabinet" alt="supprimer"
       src="Images/iconeDelete.png" height="35" width="35">
                            </a>
                            
                        </th>
                    
                        
                    </tr>
                    </thread>
                @endforeach
                </tbody>
                </table>
                
        </div>
    
    
</body>
</html>
