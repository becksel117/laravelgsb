<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Accueil</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    </head>
    
    <body>
        
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

             <li class="nav-item">
                 <div> Bienvenue {{Auth::user()->nom}} </div>
                 <a href="{{URL::route("Connexion")}}">Déconnexion</a>
            </li>
        </ul>     
    </nav> 
    <br>
        <h3 class="text-center text" name="txt">Accueil</h3>
        <div class="container p-3 my-3 px-5">
            <div class="card-deck">
                <div class="card mb-5" style="width:300px;flex: inherit;">
                   
                     <img class="card-img-top" src="{{asset('Images/iconeVisiteur.jpg')}}" style="width:100%">
                    <div class="card-body " style="text-align: center">
                        <a href="{{route("Visiteur")}}">Visiteurs</a>
                        <p class="card-text">Accéder aux informations concernant les visiteurs</p>
                    </div>
                </div>
                <div class="card mb-5" style="width:300px;flex: inherit;">
                     <img class="card-img-top" src= "{{asset('Images/iconeMedecin.jpg')}}" style="width:100%">
                    <div class="card-body " style="text-align: center">
                         <a href="{{URL::route("Medecin")}}">Médecins</a>
                        <p class="card-text">Accéder aux informations concernant les médecins</p>
                    </div>
                </div> 
                <div class="card mb-5" style="width:300px;flex: inherit;">
                     <img class="card-img-top" src="{{asset('Images/iconeRdv.png')}}" style="width:100%">
                    <div class="card-body " style="text-align: center">
                        <a href="{{URL::route("Rdv")}}">Rendez-vous</a>
                        <p class="card-text">Organisez un rendez-vous avec le médecin de votre choix 
                            afin de promouvoir votre produit</p>
                    </div>
                </div>   
                <div class="card mb-5" style="width:300px;flex: inherit;">
                     <img class="card-img-top" src="{{asset('Images/iconeProduit.svg')}}" style="width:100%">
                    <div class="card-body " style="text-align: center">
                       <a href="{{URL::route("Produit")}}">Produits</a>
                        <p class="card-text">Accéder aux informations concernant les produits</p>
                    </div>
                </div>
                <div class="card mb-5" style="width:300px;flex: inherit;">
                     <img class="card-img-top" src="{{asset('Images/iconeCabinet.png')}}" style="width:100%">
                    <div class="card-body " style="text-align: center">
                       <a href="{{URL::route("Cabinet")}}">Cabinets</a>
                        <p class="card-text">Accéder aux informations concernant les cabinets</p>
                    </div>
                </div>
                
            </div>
            
        </div>
       
    </body>
</html>


