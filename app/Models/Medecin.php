<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Medecin
 *
 * @author student
 */
class Medecin extends Model {
    
    protected $fillable = ['nom','prenom','mail', 'specialisation', 'cabId'];
}
