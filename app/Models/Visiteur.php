<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

/**
 * Description of Visiteur
 *
 * @author student
 */
class Visiteur extends Model{
    use HasFactory;
    
    protected $table = 'Visiteur';
    protected $primaryKey = 'VisId';
    protected $incrementing = true;
}
