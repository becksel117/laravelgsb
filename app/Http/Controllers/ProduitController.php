<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Models\Produit;

/**
 * Description of ProduitController
 *
 * @author student
 */
class ProduitController extends Controller {

    public function showProduit() {

        $produit = Produit::all();
        return view("Produit/Produit", ["Produit" => $produit]);
        
    }
    
    
    
    public function showAddProduit() {

        return view("Produit/AddProduit");
        
    }
    
    public function traitement()
    {
        

        produit::create([
            'id' => request('id'),
            'nom' => request('nom'),
            'prix' => request('prix')
        ]);

        return "l'ajout a bien été effectué";
    }
   
    
    public function showUpdateProduit() {

        $produit = Produit::find(id);
        return view("Produit/UpdateProduit", ["Produit" => $produit]);
        
    }
    
    public function update() {
       

        $produit = Produit::find(1);

        $produit->name = 'Paris to London';

        $produit->save();
    }
    
    public function showDeleteProduit() {

        $produit = DB::table('Produit')->get();
        return view("Produit/DeleteProduit", ["DeleteProduit" => $produit]);
        
    }
}
