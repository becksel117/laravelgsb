<?php

namespace App\Http\Controllers;

use App\Models\utilisateur;

class InscriptionController extends Controller
{
    public function formulaire()
    {
        return view('Inscription');
    }

    public function traitement()
    {
        

        utilisateur::create([
            'email' => request('email'),
            'mot_de_passe' => bcrypt(request('password')),
            'nom' => request('nom'),
            'prenom' => request('prenom'),
            'roleAdmin' => 0
        ]);

        return "Nous avons bien reçu votre email qui est " . request('email');
    }
}