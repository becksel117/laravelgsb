<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;
use Illuminate\Http\Request;
/**
 * Description of ConnexionController
 *
 * @author student
 */
class ConnexionController extends Controller
{
    public function formulaire()
    {
        auth()->logout();
        return view('Connexion');
    }

    public function traitement()
{
    request()->validate([
        'email' => ['required', 'email'],
        'password' => ['required'],
    ]);
    
    $resultat = auth()->attempt([
        'email' => request('email'),
        'password' => request('password'),
    ]);
    
    if ($resultat) {
        return redirect('/Accueil');
    }
    
    return back()->withInput()->withErrors([
        'email' => 'Vos identifiants sont incorrects.',
    ]);
    
}
}
