<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Models\Cabinet;

/**
 * Description of MedecinController
 *
 * @author student
 */
class CabinetController extends Controller {

    public function showCabinet() {

        $cabinet = Cabinet::all();
        return view("Cabinet/Cabinet", ["Cabinet" => $cabinet]);
        
    }
    
    public function traitement()
    {
        cabinet::create([
            'id' => request('id'),
            'nom' => request('nom'),
            'region' => request('region')
        ]);

        return "l'ajout a bien été effectué";
    }
    
    public function showAddCabinet() {

        return view("Cabinet/AddCabinet");
        
    }
    
    public function showUpdateCabinet() {

        $cabinet = DB::table('Cabinet')->get();
        return view("Cabinet/UpdateCabinet", ["UpdateCabinet" => $cabinet]);
        
    }
    
    public function showDeleteCabinet() {

        $cabinet = DB::table('Cabinet')->get();
        return view("Cabinet/DeleteCabinet", ["DeleteCabinet" => $cabinet]);
        
    }
}
