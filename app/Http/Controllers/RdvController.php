<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Models\Rdv;
use Illuminate\Support\Facades\Auth;


/**
 * Description of RdvController
 *
 * @author student
 */
class RdvController extends Controller {
    
    public function showRdv() {
        
        return view('Rdv/Rdv');  
    }
    
    public function traitement() {
        
        
        Rdv::create([
            'medId' => request('medId'),
            'visId' => Auth::user()->id,
            'date' => request('date'),
            'prodId' => request('prodId')
        ]);

        return "Nous avons bien reçu votre demande de Rdv";
    }

    public function showAdminRdv() {

        $rdv = DB::table('Rdv')->get();
        return view("Rdv/AdminRdv", ["AdminRdv" => $rdv]);
        
    }
    
    public function showAddRdv() {

        $rdv = DB::table('Rdv')->get();
        return view("Rdv/AddRdv", ["AddRdv" => $rdv]);
        
    }
    
    public function showUpdateRdv() {

        $rdv = DB::table('Rdv')->get();
        return view("Rdv/UpdateRdv", ["UpdateRdv" => $rdv]);
        
    }
    
    public function showDeleteRdv() {

        $rdv = DB::table('Rdv')->get();
        return view("Rdv/DeleteRdv", ["DeleteRdv" => $rdv]);
        
    }
}
