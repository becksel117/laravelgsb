<?php


namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Models\Medecin;

/**
 * Description of MedecinController
 *
 * @author student
 */
class MedecinController extends Controller {

    public function showMedecin() {

        $medecin = Medecin::all();
        return view("Medecin/Medecin", ["Medecin" => $medecin]);
        
    }
    
    public function showAddMedecin() {

        return view("Medecin/AddMedecin");
        
    }
    
    public function traitement()
    {
            produit::create([
            'id' => request('id'),
            'nom' => request('nom'),
            'prenom' => request('prenom'),
            'mail' => request('mail'),
            'specialisation' => request('specialisation'),
            'cabId' => request('cabId')
        ]);

        return "l'ajout a bien été effectué";
    }
    
    public function showUpdateMedecin() {

        $medecin = DB::table('Medecin')->get();
        return view("Medecin/UpdateMedecin", ["UpdateMedecin" => $medecin]);
        
    }
    
    public function showDeleteMedecin() {

        $medecin = DB::table('Medecin')->get();
        return view("Medecin/DeleteMedecin", ["DeleteMedecin" => $medecin]);
        
    }
    
    

}
