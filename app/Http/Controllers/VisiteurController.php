<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Models\utilisateur;

/**
 * Description of VisiteurController
 *
 * @author student
 */
class VisiteurController extends Controller {

    public function showVisiteur() {

        $visiteur = utilisateur::all();
        return view("Visiteur/Visiteur", ["Visiteur" => $visiteur]);
        
    }

     public function traitement()
    {
            utilisateur::create([
            'id' => request('id'),
            'email' => request('email'),
            'mot_de_passe' => bcrypt(request('mot_de_passe')),
            'nom' => request('nom'),
            'prenom' => request('prenom'),
            'roleAdmin' => request('roleAdmin')
    ]);
             return "l'ajout a bien été effectué";
    }
    
    public function showAddVisiteur() {

        return view("Visiteur/AddVisiteur");
        
    }
    
    public function showUpdateVisiteur() {

        $visiteur = DB::table('Visiteur')->get();
        return view("Visiteur/UpdateVisiteur", ["UpdateVisiteur" => $visiteur]);
        
    }
    
    public function showDeleteVisiteur() {

        $visiteur = DB::table('Visiteur')->get();
        return view("Visiteur/DeleteVisiteur", ["DeleteVisiteur" => $visiteur]);
        
    }

}
