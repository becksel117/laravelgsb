
<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VisiteurController;
use App\Http\Controllers\MedecinController;
use App\Http\Controllers\ProduitController;
use App\Http\Controllers\RdvController;
use App\Http\Controllers\CabinetController;
use App\Http\Controllers\ConnexionController;
use App\Http\Controllers\InscriptionController;
use App\Http\Controllers\AccueilController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ConnexionController::class, 'formulaire'])
        ->name("Connexion");


Route::get('/Inscription', [InscriptionController::class, 'formulaire'])
        ->name("Inscription");

Route::post('/Inscription', [InscriptionController::class, 'traitement'])
        ->name("Inscription");


Route::get('/Connexion', [ConnexionController::class, 'formulaire'])
        ->name("Connexion");

Route::post('/Connexion', [ConnexionController::class, 'traitement'])
        ->name("Connexion");

Route::get('/Accueil', [AccueilController::class, 'accueil'])
        ->middleware('App\Http\Middleware\Verification')
        ->name("Accueil");

Route::get('/Deconnexion', [AccueilController::class, 'deconnexion'])
        ->name("Deconnexion");

Route::get('/Visiteur', [VisiteurController::class, 'showVisiteur'])
        ->middleware('App\Http\Middleware\Verification')
        ->name("Visiteur");

Route::get('/AddVisiteur', [VisiteurController::class, 'showAddVisiteur'])
        ->middleware('App\Http\Middleware\Role')
        ->name("AddVisiteur");

Route::post('/AddVisiteur', [VisiteurController::class, 'traitement'])
        ->middleware('App\Http\Middleware\Role')
        ->name("AddVisiteur");

Route::get('/UpdateVisiteur/{id}', [VisiteurController::class, 'showUpdateVisiteur'])
        ->name("UpdateVisiteur");

Route::get('/DeleteVisiteur/{id}', [VisiteurController::class, 'showDeleteVisiteur'])
        ->name("DeleteVisiteur");

Route::get('/Medecin', [MedecinController::class, 'showMedecin'])
        ->middleware('App\Http\Middleware\Verification')
        ->name("Medecin");

Route::get('/AddMedecin', [MedecinController::class, 'showAddMedecin'])
        ->middleware('App\Http\Middleware\Role')
        ->name("AddMedecin");

Route::post('/AddMedecin', [MedecinController::class, 'traitement'])
        ->middleware('App\Http\Middleware\Role')
        ->name("AddMedecin");

Route::get('/UpdateMedecin/{id}', [MedecinController::class, 'showUpdateMedecin'])
        ->name("UpdateMedecin");

Route::get('/DeleteMedecin/{id}', [MedecinController::class, 'showDeleteMedecin'])
        ->name("DeleteMedecin");

Route::get('/Rdv', [RdvController::class, 'showRdv'])
        ->middleware('App\Http\Middleware\Verification')
        ->name("Rdv");

Route::post('/Rdv', [RdvController::class, 'traitement'])
        ->middleware('App\Http\Middleware\Verification')
        ->name("Rdv");

Route::get('/AddRdv', [RdvController::class, 'showAddRdv'])
        ->middleware('App\Http\Middleware\Role')
        ->name("AddRdv");

Route::get('/UpdateRdv/{idMed}/{idVis}/{date}', [RdvController::class, 'showUpdateRdv'])
        ->name("UpdateRdv");

Route::get('/DeleteRdv/{idMed}/{idVis}/{date}', [RdvController::class, 'showDeleteRdv'])
        ->name("DeleteRdv");

Route::get('/Produit', [ProduitController::class, 'showProduit'])
        ->middleware('App\Http\Middleware\Verification')
        ->name("Produit");

Route::get('/AddProduit', [ProduitController::class, 'showAddProduit'])
        ->middleware('App\Http\Middleware\Role')
        ->name("AddProduit");

Route::post('/AddProduit', [ProduitController::class, 'traitement'])
        ->middleware('App\Http\Middleware\Role')
        ->name("AddProduit");

Route::get('/UpdateProduit/{id}', [ProduitController::class, 'showUpdateProduit'])
        ->name("UpdateProduit");

Route::get('/DeleteProduit/{id}', [ProduitController::class, 'showDeleteProduit'])
        ->name("DeleteProduit");

Route::get('/Cabinet', [CabinetController::class, 'showCabinet'])
        ->middleware('App\Http\Middleware\Verification')
        ->name("Cabinet");

Route::get('/AddCabinet', [CabinetController::class, 'showAddCabinet'])
        ->middleware('App\Http\Middleware\Role')
        ->name("AddCabinet");

Route::post('/AddCabinet', [CabinetController::class, 'traitement'])
        ->middleware('App\Http\Middleware\Role')
        ->name("AddCabinet");

Route::get('/UpdateCabinet/{id}', [CabinetController::class, 'showUpdateCabinet'])
        ->name("UpdateCabinet");

Route::get('/DeleteCabinet/{id}', [CabinetController::class, 'showDeleteCabinet'])
        ->name("DeleteCabinet");