<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRdvsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rdvs', function (Blueprint $table) {
              $table->unsignedInteger('medId');
    $table->unsignedInteger('visId');
    $table->dateTime('date');
    $table->unsignedInteger('prodId');
    $table->timestamps();
    $table->foreign('medId')
                ->references('id')
                ->on('medecins');
    $table->foreign('visId')
                ->references('id')
                ->on('utilisateurs');
    $table->foreign('prodId')
                ->references('id')
                ->on('produits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rdvs');
    }
}
