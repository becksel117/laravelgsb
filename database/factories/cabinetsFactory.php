<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;
use App\Http\Models\Cabinet;

$factory->define(Cabinet::class, function (Faker $faker) {
    return [
        'nom'         => $faker->name,
        'region'   => $faker->Str::random(10),
    ];
});