<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Database\Factories\cabinetsFactory;


class CabinetTableSeeder extends Seeder{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        cabinetsFactory::factory()->count(10)->create();
    }
}